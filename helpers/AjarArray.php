<?php
	namespace osoyan\ajar\helpers;

	class AjarArray
	{
		public $array = array();

		public function push()
		{
			$args = func_get_args();
			for ($i = 0; $i < count($args); $i++)
				array_push($this->array, $args[$i]);

			return $this;
		}

		public function split($string, $separator)
		{
			$this->array = explode($separator, $string);
			return $this;
		}

		public function join($separator)
		{
			return implode($separator, $this->array);
		}
	}
?>
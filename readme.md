Controller
=============
    $this->getModel($modelName) -> Load model by its name. From project/%project_name%/engine/models

    $this->getLib($libName) -> Create and return instance of library. The libs are in libs folder.

    $this->show($vars, $layout, $view = null) -> Load view in buffer and return it. Default view name gets from
    controller name. $vars will be used in the pattern.

Core
=============
    self::$project -> contains current project name

    self::getConfig() -> load project.json file and parse it and return the content.
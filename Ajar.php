<?php
	namespace osoyan\ajar;

	use osoyan\ajar\system\Core;
	use osoyan\modules\dbedit\controllers\MainController;

	class Ajar
	{
		public function __construct($project)
		{
			Core::$project = $project;
		}

		public function run()
		{
			// Get url and split all sections into array
			$path = explode("/", substr($_SERVER['REQUEST_URI'], 1));
			$module = array_shift($path);
			$controller = array_shift($path);
			$method = array_shift($path);
			$error_stack = array();

			// Default Method
			if (empty($method)) $method = "index";

			// Check module existing
			if (!file_exists("../../projects/".Core::$project."/engine/modules/".$module."/Module.php"))
				array_push($error_stack, "Module <b>$module</b> not found!");

			// Check controller existing
			if (!file_exists("../../projects/".Core::$project."/engine/modules/".$module."/controllers/".ucfirst($controller)."Controller.php"))
				array_push($error_stack, "Controller <b>$controller</b> not found!");

			// If we don't have errors
			if (empty($error_stack))
			{
				require_once("../../projects/".Core::$project."/engine/modules/".$module."/controllers/".ucfirst($controller)."Controller.php");

				$ctrl_real = ucfirst($controller)."Controller";
				$ctrl = new $ctrl_real();
				$ctrl->name = $controller;
				$ctrl->method = $method;
				$ctrl->module = $module;

				if (method_exists($ctrl_real, "action".ucfirst($method))) @call_user_func_array(array($ctrl, "action".ucfirst($method)), $path);
				else die("Method <b>$method</b> not found!");
			} else {
				// If we were unable to load the module, we will try to load the module from special directory
				if (!file_exists(__DIR__."/../modules/".$module."/Module.php"))
					die("Module <b>$module</b> not found!");

				if (!file_exists(__DIR__."/../modules/".$module."/controllers/".ucfirst($controller)."Controller.php"))
					die("Controller <b>$controller</b> not found!");

				$ctrl_real = "osoyan\\modules\\${module}\\controllers\\" . ucfirst($controller) . "Controller";

				$ctrl = new $ctrl_real();
				$ctrl->name = $controller;
				$ctrl->method = $method;
				$ctrl->module = $module;

				if (method_exists($ctrl_real, "action".ucfirst($method))) @call_user_func_array(array($ctrl, "action".ucfirst($method)), $path);
				else die("Method <b>$method</b> not found!");
			}
		}
	}
?>
<?php
	class AjarLoader
	{
		public static function autoload($class)
		{
			$className = ltrim($class, '\\');
			$fileName  = '';
			$namespace = '';

			if ($lastNsPos = strrpos($className, '\\'))
			{
				$namespace = substr($className, 0, $lastNsPos);
				$className = substr($className, $lastNsPos + 1);

				$firstSpace = explode("\\", $namespace);
				if ($firstSpace[0] == "app") $firstSpace[0] = "projects\\".\osoyan\ajar\system\Core::$project;
				else return;

				$namespace = implode("\\", $firstSpace);
				$fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
			}

			$fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

			$dir = __DIR__;
			$dir = str_replace("vendor\\osoyan\\ajar", "", $dir);

			if (file_exists($dir.$fileName)) {
				//echo $fileName."<br>";
				require_once($dir.$fileName);
			}
		}
	}

	spl_autoload_register(['AjarLoader', 'autoload'], true, true);
?>
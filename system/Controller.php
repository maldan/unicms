<?php
	namespace osoyan\ajar\system;

	class Controller
	{
		public $name;
		public $method;
		public $pageInfo;
		public $module;

		public function getModel($modelName)
		{
			if (file_exists("../../projects/".Core::$project."/engine/modules/".$this->module."/models/".ucfirst($modelName)."Model.php"))
				require_once("../../projects/".Core::$project."/engine/modules/".$this->module."/models/".ucfirst($modelName)."Model.php");

			if (file_exists(__DIR__."/../../modules/".$this->module."/models/".ucfirst($modelName)."Model.php"))
				require_once(__DIR__."/../../modules/".$this->module."/models/".ucfirst($modelName)."Model.php");

			if (!file_exists("../../projects/".Core::$project."/engine/modules/".$this->module."/models/".ucfirst($modelName)."Model.php")
			&& !file_exists(__DIR__."/../../modules/".$this->module."/models/".ucfirst($modelName)."Model.php"))
				die("Model <b>".ucfirst($modelName)."</b> not found!");

			$modelName = ucfirst($modelName)."Model";
			return new $modelName();
		}

		public function getLib($libName)
		{
			$class = "osoyan\\ajar\\libs\\${libName}\\${libName}";
			return new $class();
		}

		public function getConnection()
		{
			return new DataBase();
		}

		public function setPageInfo($title, $description, $keywords, $lang, $document_state, $robots)
		{
			$this->pageInfo["title"] = $title;
			$this->pageInfo["description"] = $description;
			$this->pageInfo["keywords"] = $keywords;
			$this->pageInfo["lang"] = $lang;
			$this->pageInfo["document-state"] = $document_state;
			$this->pageInfo["robots"] = $robots;
		}

		public function show($data, $layout, $view = null)
		{
			$path = $this->method;

			if ($this->name !== "main") $path = $this->name."/".$this->method;
			if (isset($view)) $path = $view;

			foreach ($data as $key => $value)
				${$key} = $value;

			// Load pattern
			ob_start();
			require_once("../../projects/".Core::$project."/engine/modules/".$this->module."/views/${path}.php");
			$bodyContent = ob_get_contents();
			ob_clean();

			// Load layout
			ob_start();
			require_once("../../projects/".Core::$project."/engine/modules/".$this->module."/layouts/${layout}.php");
			ob_end_flush();
			$layoutData = ob_get_contents();
			ob_clean();

			return $layoutData;
		}
	}
?>
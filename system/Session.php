<?php
	namespace osoyan\ajar\system;

	class Session
	{
		public static function set($key, $val)
		{
			@session_start();
			$_SESSION[$key] = $val;
		}

		public static function get($key)
		{
			@session_start();
			return $_SESSION[$key];
		}
	}
?>
<?php

	namespace osoyan\ajar\system;

	class DataBase
	{
		public $connection;

		public function autoConnect()
		{
			$params = Core::getConfig();

			if (!isset($params[$_SERVER['SERVER_NAME']])) die("DataBase parameters not found for domain <b>".$_SERVER['SERVER_NAME']."</b>");

			$params_domain = $params[$_SERVER['SERVER_NAME']];

			$this->connection = mysqli_connect(
				$params_domain["dbHost"],
				$params_domain["dbUser"],
				$params_domain["dbPass"],
				$params_domain["dbName"]
			);

			return $this->connection;
		}
	}
?>
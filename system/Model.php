<?php

	namespace osoyan\ajar\system;

	class Model
	{
		public $mysqli;

		public function setConnection($connectionLink)
		{
			$this->mysqli = $connectionLink;
			mysqli_set_charset($this->mysqli, "utf8");
		}

		public function setAutoConnection()
		{
			$db = new DataBase();
			$this->mysqli = $db->autoConnect();
			mysqli_set_charset($this->mysqli, "utf8");
		}

		public function query($q)
		{
			return mysqli_query($this->mysqli, $q);
		}

		public function fetch($q)
		{
			return mysqli_fetch_assoc($q);
		}

		public function isNull($r)
		{
			return !(bool)mysqli_num_rows($r);
		}

		public function one($r)
		{
			$rj = mysqli_fetch_assoc($r);
			return $rj;
		}

		public function lastID()
		{
			return mysqli_insert_id($this->mysqli);
		}

		public function each($r, $f)
		{
			while ($rj = mysqli_fetch_assoc($r)) {
				$f($rj);
			}
		}
	}
?>
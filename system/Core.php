<?php
	namespace osoyan\ajar\system;

	class Core
	{
		public static $project;

		public static function getConfig()
		{
			return json_decode(file_get_contents("../../config/ajar/".Core::$project.".json"), true);
		}
	}
?>
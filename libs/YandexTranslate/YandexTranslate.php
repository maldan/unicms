<?php
	namespace osoyan\ajar\libs\YandexTranslate;

	class YandexTranslate
	{
		public $api_key = "trnsl.1.1.20150824T200814Z.5eefcf0808926bdb.b158fed6277061d58e093674aa9b482105efa2a5";

		public function getTranslate($fromTo, $word)
		{
			return json_decode(file_get_contents("https://translate.yandex.net/api/v1.5/tr.json/translate?key=".$this->api_key."&text=".$word."&lang=".$fromTo), true);
		}
	}
?>